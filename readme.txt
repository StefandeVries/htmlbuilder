######################################################################################################################################
HTMLbuilder read-me
######################################################################################################################################
Use this software only under the terms of GNU GPL v3.  Its terms can be found in gplv3.txt
######################################################################################################################################
The css file in use should *at least* have the following classes and ids:

Classes: 

t		plain text, non-header
h*		since header is a line preceded by one or more header characters - which in my case is '~' - you should add a class definition
		of all the different types you use.  I only use headers up to four ~'s, so I end up with a stylesheet containing header classes 
		h, hh, hhh and hhhh

IDs:

content 	the content <div>
navbar 		the navbar <div>
######################################################################################################################################
In this version, you can also use unordered lists.  Try prepending lines with a '|' to make an unordered list, like this:
	| My epic page
	| Great something
You cannot yet use headers and list items together.
######################################################################################################################################
To test if it is all working, call HTMLbuilder on the test file named 'index', linking it to the provided stylesheet fermata.css:
htmlbuilder index fermata.css
######################################################################################################################################
